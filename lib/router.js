Router.configure({
  layoutTemplate: 'main'
});

Router.route('/', function() {
  this.render('header', {to: 'header'});
  this.render('home');
  this.render('footer', {to: 'footer'});
});

Router.route('/contact-us', function() {
  this.render('header', {to: 'header'});
  this.render('contactus');
  this.render('footer', {to: 'footer'});
});

Router.route('/admin', function() {
  this.render('adminheader', {to: 'header'});
  this.render('content');
  this.render('adminfooter', {to: 'footer'});
});

Router.route('/login', function() {
  this.render('login');
});

Router.route('/product-detail/:_id', {

  //var item = Products.findOne({_id: this.params._id});
  template: 'product_detail',

  data: function() {

    //console.log("this.params._id++++++++=> ",this.params._id);
    // getProductdetail
    Meteor.subscribe("getProductdetail", this.params._id);
    // return Products.findOne(this.params._id);
    console.log("--  Products ****** --",Products.find({_id:new Meteor.Collection.ObjectID(this.params._id)}));
    return Products.find({_id:new Meteor.Collection.ObjectID(this.params._id)});
  },

  action: function() {
    // render all templates and regions for this route
    this.render();
  }

});

Router.route('/register', function() {
  this.render('header', {to: 'header'});
  this.render('register');
  this.render('footer', {to: 'footer'});
});

Router.route('/addproduct', function() {
  this.render('header', {to: 'header'});
  this.render('addproduct');
  this.render('footer', {to: 'footer'});
});
