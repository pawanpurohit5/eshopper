Meteor.methods({

    addProduct: function(product){
        // if(!Meteor.userId()){
        //   throw new Meteor.Error("not-authorized");
        // }
    
    Products.insert({
        "title" : product.title, 
        "description" :product.description, 
        "category" : [ product.category ], 
        "brand" : [ product.brand ], 
        "images" : [ "image01.png", "image02.png" ],
        "details" : product.details ,
        "price" : product.price,
        "color" : [ "Black", "Silver", "Golden" ], 
        "featured" : true, 
        "status" : true, 
        "created_at" : "2016-04-29T12:53:40.201Z" 
    });
  }
});