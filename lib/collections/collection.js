import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

Categories = new Mongo.Collection('categories');
Products = new Mongo.Collection('products');
Enquiry = new Mongo.Collection('enquiry');
//Product = new Mongo.Collection("products");


Enquiry.attachSchema(new SimpleSchema({
  name: {
    type: String,
    label: "name",
    max: 200
  },
  email: {
    type: String,
    label: "email",
    regEx: SimpleSchema.RegEx.Email
  },
  subject: {
    type: String,
    label: "subject"

  },
  message: {
    type: String,
    label: "message"

  }
}));

Enquiry.allow({
  insert: function() {
    // only allow posting if you are logged in
    return 'Thank you for contacting us.';
  },

});


if (Meteor.isServer) {

  Meteor.publish('categories', function categoryPublication() {
    return Categories.find();
  });

  Meteor.publish('products', function productPublication() {
    return Products.find({ "status": true });
  });

  Meteor.publish('latest_product', function latestproductPublication() {
    return Products.find({ "status": true }, { sort: { created_at: -1 }, limit: 6 });
  });

  Meteor.publish('getProductdetail', function productdetailPublication(productId) {
    console.log("================ getProductdetail publish called=========", productId);
    console.log("products get ", Products.find({ _id:new Meteor.Collection.ObjectID(productId)}).fetch());                                
    return Products.find({ _id:new Meteor.Collection.ObjectID(productId)});

  });

}

Meteor.methods({


});
