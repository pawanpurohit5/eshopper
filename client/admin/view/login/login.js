Template.login.events({
    'submit form': function(event){
        event.preventDefault();
        var emailVar = $("#inputEmail").val();
        var passwordVar = $('#inputPassword').val();
        var details=Meteor.loginWithPassword(emailVar, passwordVar,function(error){
    		if(error){
    			$("#messageContainer").css("display","flex");
    			$("#messageContainer").html(error.reason);
				console.log("You initiated the login process.",error.reason)
		    } else {
		        Router.go('/');
		    }
    	});
    	console.log("details---------",details)
    },
    'click .logout': function(event){
        event.preventDefault();
        Meteor.logout();
        Router.go('/');
    }
});