Template.adminheader.helpers({
  firstName: function() {
    console.log("--currentuser--",Meteor.users().profile.fname);
    return Meteor.user().profile.fname;
  }
});
Template.adminheader.events({
    'submit form': function(event){
        event.preventDefault();
        var emailVar = $("#inputEmail").val();
        var passwordVar = $('#inputPassword').val();
        var details=Meteor.loginWithPassword(emailVar, passwordVar,function(error){
    		if(error){
    			$("#messageContainer").css("display","flex");
    			$("#messageContainer").html(error.reason);
				console.log("You initiated the login process.",error.reason)
		    } else {
		        Meteor._reload.reload();  
		    }
    	});
    	console.log("details---------",details)
    },
    'click .logout': function(event){
        event.preventDefault();
        Meteor.logout();
        Router.go('/');
    }
});