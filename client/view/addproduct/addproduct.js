Template.addproduct.onRendered(function(){
    $('.addproduct').validate();
});

Template.addproduct.events({
  "submit .add-stock": function(event) {
    event.preventDefault();
	var product = {
		title :event.target.title.value,
		name :event.target.name.value,
		email : event.target.email.value,
		category :event.target.category.value,
		brand :event.target.brand.value,
		price : event.target.price.value,
		details : event.target.details.value,
		image : event.target.image.value,
		description : event.target.description.value
	};

	Meteor.call("addProduct", product);
   	
   	event.target.title.value='';
	event.target.name.value ='';
	event.target.email.value ='';
	event.target.category.value ='';
	event.target.brand.value ='';
	event.target.price.value ='';
	event.target.details.value ='';
	event.target.description.value ='';
	event.target.image.value='';


	alert("product inserted");
   
  }
});