Template.register.onRendered(function(){
    $('.regi').validate();
});

Template.register.events({
    'submit form': function(event){
        event.preventDefault();

        var title = $('[name=title]').val();
        var fname = $('[name=fname]').val();
        var lname = $('[name=lname]').val();
        var company = $('[name=company]').val();
        var address = $('[name=address]').val();
        var address2 = $('[name=address2]').val();
        var city = $('[name=city]').val();
        var state = $('[name=state]').val();
        var postcode = $('[name=postcode]').val();
        var country = $('[name=country]').val();
        var aditionalInfo = $('[name=aditionalInfo]').val();
        var mobile = $('[name=mobile]').val();
        var email = $('[name=email]').val();
        var password = $('[name=pwd1]').val();

        Accounts.createUser({
        	email: email,
            password: password,
			profile: {
		        firstName: fname,
		        firstName: lname,
		        title:title,
				company:company,
				address:address,
				address2:address2,
				city:city,
				state:state,
				postcode:postcode,
				country:country,
				aditionalInfo:aditionalInfo,
				mobile:mobile,
			    IsActive: 0
		        }
        });
        console.log("User Data Store");
        Router.go("/");
	}
});
