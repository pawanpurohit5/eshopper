import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { ReactiveDict } from 'meteor/reactive-dict';



Template.sidebar.onCreated(function categorylistCreated() {

	Meteor.subscribe('categories');

});


Template.sidebar.helpers({

	categorylist() {

		return Categories.find();

	},
  
});

Template.sidebar.events({
  
});
