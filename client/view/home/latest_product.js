Template.latest_product.onCreated(function latestproductCreated() {

  Meteor.subscribe('latest_product');
  //console.log('--product list--',Meteor.subscribe('latest_product'));

});


Template.latest_product.helpers({
  latest_product() {
    //console.log(Products.find({"status":true},{sort:{created_at:-1}, limit:6}));
    return Products.find({ "status": true }, { sort: { created_at: -1 }, limit: 6 });
  },

  idToString: function(objectValue) {
    if (typeof objectValue !== "undefined" && typeof objectValue._id !== "undefined") {      
      return objectValue._id._str;
    }
    return "";
  }
});

Template.latest_product.events({

});
